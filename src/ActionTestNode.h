#pragma once
//======================================================================================================================
// 2019 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
#include <ActionNode.h>
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
class ActionTestNode : public ActionNode {
public:
    //---------------------------------------------------------------------------------------------------------------------
    ActionTestNode(std::string const& name, int const bit_pattern) :
        ActionNode(name, __FUNCTION__), mBits(bit_pattern) {
    }
    ActionTestNode(ActionTestNode const& atn) :
        ActionTestNode { atn.mName, atn.mBits } {
    }
    virtual ~ActionTestNode() = default;

    //---------------------------------------------------------------------------------------------------------------------
    virtual TreeNode::NodeState const eval() override {
        Depth depth;
        mys::tout << depth << getNodeText();

        if (mRunning) {
            setState(Running);
            --i;
        }
        else if ((i & mBits) == mBits) {
            setState(Success);
        }
        else {
            setState(Failure);
        }
        usleep(500);
        ++i;
        mRunning = !mRunning;

        return getState();
    }

private:
    int i {};
    int mBits;
    bool mRunning { true };
};

