#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//
//======================================================================================================================
#include <ConditionNode.h>
using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class ConditionTestNode : public ConditionNode {
public:
    ConditionTestNode(std::string const& name, int const bit_pattern) :
        ConditionNode::ConditionNode(name, __FUNCTION__), mBits(bit_pattern) {
    }
    ~ConditionTestNode() = default;

    virtual NodeState const eval() override {
        Depth depth;
        mys::tout << depth << getNodeText();

        setState(Running);

        if ((i & mBits) == mBits) {
            setState(Success);
        }
        else {
            setState(Failure);
        }

        usleep(500);
        ++i;

        return getState();
    }

private:
    int i {};
    int mBits;
};

