//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//       File: test_trace.cpp
//
//     Author: rmerriam
//
//    Created: Oct 24, 2021
//
//======================================================================================================================
#include <tut/tut.hpp>
#include <tut/tut_reporter.hpp>

namespace tut {
    test_runner_singleton runner;
}
//--------------------------------------------------------------------------------------------------------------------------
int main() {

    tut::reporter reporter;
    tut::runner.get().set_callback( &reporter);

    tut::test_result tr;

    tut::runner.get().run_tests("BT Test Leaf");

    return !reporter.all_ok();
}
