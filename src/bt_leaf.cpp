//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//       File: test_pod.cpp
//
//     Author: rmerriam
//
//    Created: Oct 24, 2021
//
//======================================================================================================================
#include <memory>
#include <tut/tut.hpp>

#include <BehaviorTree.h>
#include "ActionTestNode.h"
#include "ConditionTestNode.h"
using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
struct bt_test_data {

    ActionTestNode task_1 { "task 1", 1 }; //
    ActionTestNode task_2 { "task 2", 2 }; //
    ActionTestNode task_3 { "task 3", 1 }; //

    ConditionTestNode cond_test { "test X", 1 };

    BT::FailNode fail { task_3 }; //
    BT::InverterNode invert { task_1 }; //
    BT::ResetNode reset { task_1 }; //
    BT::SucceedAlwaysNode succeed_always { task_1 }; //
    BT::SucceedOnceNode succeed_once { task_1 }; //

    std::unique_ptr<BT::TreeNode> composite { //

//        new BT::IteratorNode { "task seq", //
//            TreeVect { //
//            task_1, //
//                task_2, //
//                task_3 //
//            } }
    //
    //        new BT::SequenceNode { "task seq", //
    //            TreeVect { //
    //            task_1, //
    //                task_2, //
    //                task_3 //
    //            } }
    //
//                new BT::SelectorNode { "task seq", //
//                    TreeVect { //
//                    task_1, //
//                        task_2, //
//                        task_3 //
//                    } }
    //
    //
//        new BT::MemSeqNode { "task memseq", //
//            TreeVect { //
//            task_1, //
//                task_2, //
//                task_3 //
//            } //
//        }
    //
    //        new BT::MemSelNode { "task memsel", //
    //            TreeVect { //
    //            task_1, //
    //                task_2, //
    //                task_3 //
    //            }//
    //        }
    //
//        new BT::IteratorNode { "task memsel", //
//            TreeVect { //
//            task_1, //
//                task_2, //
//                task_3 //
//            }//
//        }
    //
    //
    //        new BT::MemSelNode { "task memsel", //
    //            TreeVect { //
    //            task_1, //
    //                task_2, //
    //                task_3 //
    //            }//
    //        }
    };
};
//=====================================================================================================================
namespace tut {

    typedef test_group<bt_test_data> tg;
    tg bt_leaf("BT Test Leaf");

    typedef tg::object bt_test;
    //=====================================================================================================================
    // bool

    template <>
    template <>
    void bt_test::test<1>() {

        for (auto i = 0; i < 16; ++i) {
//            composite->eval();
//            task_1.eval();
            cond_test.eval();
            //            fail.eval();
//            invert.eval();
//            reset.eval();
            succeed_always.eval();
            succeed_once.eval();
        }
//        set_test_name("bool output");
//        std::string td("false");
//
//        bool const test_data { false };
//
//        tstrm << test_data;
//        mys::ttmp << code_line << test_output() << mys::nl;
//        ensure_equals("bool false trace failed", test_output(), td);
    }

}    // namespace end
